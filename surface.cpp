#include <iostream>
#include <vector>

using namespace std;

class Surface {
public:
  string name;
  double length;
  double width;

  Surface(const string &name_, double length_, double width_) : name(name_), length(length_), width(width_) {}

  double get_area() const {
    return length * width;
  }
};

int main() {
  vector<Surface> surfaces;

  surfaces.emplace_back("aaa", 3, 3);
  surfaces.emplace_back("bbb", 1, 1);
  surfaces.emplace_back("ccc", 2, 2);

  cout << "Antall flater: " << surfaces.size() << endl;

  for (auto &flate : surfaces)
    cout << flate.name << " areal: " << flate.get_area() << endl;
}
