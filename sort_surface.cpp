#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

class Surface {
public:
  string name;
  double length;
  double width;

  Surface(const string &name_, double length_, double width_) : name(name_), length(length_), width(width_) {}

  double get_area() const {
    return length * width;
  }
};

int main() {
  vector<Surface> surfaces;

  surfaces.emplace_back("aaa", 2, 2);
  surfaces.emplace_back("bbb", 1, 1);
  surfaces.emplace_back("ccc", 3, 3);

  cout << "Antall flater: " << surfaces.size() << endl;

  for (auto &surface : surfaces)
    cout << surface.name << " areal: " << surface.get_area() << endl;

  sort(surfaces.begin(), surfaces.end(), [](const Surface &a, const Surface &b) -> bool {
    return a.get_area() < b.get_area();
  });

  cout << endl
       << "Sortert etter areal:" << endl;

  for (auto &surface : surfaces)
    cout << surface.name << " areal: " << surface.get_area() << endl;
}
