#include <iostream>
#include <vector>

using namespace std;

int main() {
  vector<int> numbers;

  numbers.emplace_back(3);
  numbers.emplace_back(1);
  numbers.emplace_back(2);

  sort(numbers.begin(), numbers.end());

  for (auto &number : numbers)
    cout << number << endl;
}
